// Глобальные переменные
const startPeopleCount = 5;
let peopleCount = startPeopleCount;
let peoples = {};
const createPeopleButton = document.getElementById('add-human');
const deletePeopleButton = document.getElementById('delete-human');
let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];
let additionalPeoples = 0;

// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имения
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
createPeoples = (peopleCount) => {
    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor()
        };
    }
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной
// TODO: возможно стоит поменять название на более подходящее
displayPeoples = (peoplesForDisplay) => {
    wrapperPeoples.innerHTML = '';

    for (let people in peoplesForDisplay) {
        let peopleElement = document.createElement('i');

        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesForDisplay[people].id);
        peopleElement.setAttribute('data-name', peoplesForDisplay[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[people].color);

        wrapperPeoples.appendChild(peopleElement);

        peopleElement.addEventListener('click', clickedByPeople);
        peopleElement.addEventListener('contextmenu', deletePeopleByClick);

        peoples[people].domElement = peopleElement;
    }
};

// Изменение цвета человекчка
function changePeopleColor(people, color) {
    people.style.color = getRandomColor();
}

getDomPeopleById = (peopleId) => {
    return peoples[peopleId].domElement;
};

// Функция, которая срабатывает при клике на человечка
clickedByPeople = (e) => {
    let people = e.target;
    let newColor = getRandomColor();
    changePeopleColor(people, newColor);
    peoples[people.attributes.getNamedItem('data-id').value].color = newColor;
};

//Создаём нового человечка
function createNewPeople(newPeopleId, name, color) {
    peoples[newPeopleId] = {
            id: newPeopleId,
            name: name,
            color: color
        };
}

//Показываем нового человечка
function displayNewPeople() {
    let newPeopleId;
    
    if (Object.keys(peoples).length < 1){
        console.log(true);
        console.log(peoples);
        newPeopleId = 1;
    } else {
        console.log(false);
        console.log(Object.keys(peoples).length);
        newPeopleId = ++wrapperPeoples.lastChild.attributes.getNamedItem('data-id').value;
    }
    
    let name = generateName();
    let color =  getRandomColor();
    createNewPeople(newPeopleId, name, color);
    displayPeoples(peoples);
}

//Удаляем последнего
function deleteLastPeople(lastPeopleId) {
    if (Object.keys(peoples).length < 1) {
        peopleCount = 1;

        return peopleCount;
    }
    wrapperPeoples.lastChild.remove();
    delete peoples[peopleCount];
    --peopleCount;
}

//Удаляем по клику
function deletePeopleByClick(e) {
    e.preventDefault();
    delete peoples[e.target.attributes.getNamedItem('data-id').value];
    e.target.remove();
    --peopleCount;
    
}

// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
domContentLoaded = () => {
    console.log(peopleCount);
    createPeoples(startPeopleCount);
    displayPeoples(peoples);
};

deletePeopleButton.addEventListener('click', deleteLastPeople);
createPeopleButton.addEventListener('click', displayNewPeople);
document.addEventListener('DOMContentLoaded', domContentLoaded);
