const buttonResult = document.getElementById('result_button');
const inputNumbers = document.getElementById('text');

function findMedian(sortedArray) {
    let numbers = getNumbers();
    let median;
    let middle = Math.floor(numbers.length / 2);

    if(numbers.length % 2 === 1) {
        median = numbers[middle];
    } else {
        median = (numbers[middle-1] + numbers[middle]) / 2;
    };

    return median;
};

function getNumbers() {
    let stringFromInpunt = inputNumbers.value;
    return stringFromInpunt;
};

function convertToArray(stringFromInpunt) {
    stringFromInpunt = stringFromInpunt.split(",");

    for (let i = 0; i < stringFromInpunt.length; i++) {
        stringFromInpunt[i] = +stringFromInpunt[i]
    };

    return stringFromInpunt;
};

function bubbleSort(nubmersArray) {
    let temp;

    for (let j = 0; j < nubmersArray.length; j++) {
        for (let i = 0; i < nubmersArray.length; i++) {
            
            if (nubmersArray[i] > nubmersArray[i+1]) {
                temp = nubmersArray[i+1];
                nubmersArray[i+1] = nubmersArray[i];
                nubmersArray[i] = temp;
            }   
        }
    }
    return nubmersArray;
};

function showResult(sortedArray,median) {
    let answer = 'начальный массив: '+inputNumbers.value
                +'<br> отсортированный массив: '+sortedArray
                +'<br> медиана: '+median;
                
    document.getElementById('result').innerHTML = answer;
};

buttonResult.onclick = function() {
    let stringFromInpunt = getNumbers();
    let numbersArray = convertToArray(stringFromInpunt);
    let sortedArray = bubbleSort(numbersArray);
    let median = findMedian(sortedArray);
    showResult(sortedArray,median);
};
