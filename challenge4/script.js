
const plusButton = document.getElementById('plus-1');
const minusButton = document.getElementById('minus-1');
const score = document.getElementById('score-1');
const addCounterButton = document.getElementById('add_counter');
let counterCount = 1

function validation(currentScore) {
    if (currentScore < 0 || currentScore > 10) {
        return 0;
    }

    return currentScore;
}

function minusOnClick(e) {
    let currentScore = +this.parentElement.children[1].innerHTML;

    currentScore = validation(--currentScore);
    this.parentElement.children[1].innerHTML = currentScore;
}

function plusOnClick(e) {
    let currentScore = +this.parentElement.children[1].innerHTML;

    currentScore = validation(++currentScore);
    this.parentElement.children[1].innerHTML = currentScore;
}

function keyboardPlus() {
    let currentScore = +score.innerHTML;

    score.innerHTML = validation(++currentScore);
}

function keyboardMinus() {
    let currentScore = +score.innerHTML;

    score.innerHTML = validation(--currentScore);
}

function keyPress(e) {
    switch(e.keyCode) {

    case 39:
        keyboardPlus();
        break;

    case 37:
        keyboardMinus();
        break;
    }
}

function addCounter() {
    ++counterCount;
    let counterContainer = document.createElement('div');
    counterContainer.className = 'counter';
    counterContainer.id = counterCount;

    let inputMinus = document.createElement('input');
    inputMinus.type = 'button';
    inputMinus.value = '-';
    inputMinus.id = 'minus-'+counterCount;
    inputMinus.className = 'button';
    inputMinus.addEventListener('click',minusOnClick);

    let inputPlus = document.createElement('input');
    inputPlus.type = 'button';
    inputPlus.value = '+';
    inputPlus.id = 'plus-'+counterCount;
    inputPlus.className = 'button';
    inputPlus.addEventListener('click',plusOnClick)

    let divScore = document.createElement('div');
    divScore.innerHTML = 0;
    divScore.id = 'score-'+counterCount;
    divScore.className = 'score';

    document.getElementById('counters').appendChild(counterContainer)
    document.getElementById(counterCount).appendChild(inputMinus)
    document.getElementById(counterCount).appendChild(divScore)
    document.getElementById(counterCount).appendChild(inputPlus);

}

window.addEventListener('keydown', keyPress);
addCounterButton.addEventListener('click',addCounter);
plusButton.addEventListener('click',plusOnClick);
minusButton.addEventListener('click',minusOnClick);