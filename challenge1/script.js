
const buttonResult = document.getElementById('result_button');

const inputNumber1 = document.getElementById('number1');
const inputNumber2 = document.getElementById('number2');
const inputNumber3 = document.getElementById('number3');

const result = document.getElementById('result');

buttonResult.onclick = function() {

    let value1 = +inputNumber1.value;
    let value2 = +inputNumber2.value;
    let value3 = +inputNumber3.value;

    let maxValue = value1;
    let minValue = value1;

    if (value2 > maxValue) {
        maxValue = value2;
    }

    if (value3 > maxValue) {
        maxValue = value3;
    }

    if (value2 < minValue) {
        minValue = value2
    }

    if (value3 < minValue) {
        minValue = value3
    }

    let median = (value1 + value2 + value3) - minValue - maxValue;

    result.innerHTML = '<h1>'+median+'</h1>'

};
