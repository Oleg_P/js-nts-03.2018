const plusButton = document.getElementById('plus');
const minusButton = document.getElementById('minus');
const score = document.getElementById('score');

function getCurrentScore() {

    return +score.innerHTML;
};

function decrementByOne(currentScore) {
    
    return --currentScore;
};

function incrementByOne(currentScore) {

    return ++currentScore;
};

function avoidNegativeScore(currentScore) {
    
    if (currentScore < 0) {
        return 0;
    }

    return currentScore;
};

minusButton.onclick = function() {
    let t1 = performance.now();
    let currentScore = getCurrentScore();
    currentScore = decrementByOne(currentScore);
    currentScore = avoidNegativeScore(currentScore);
    score.innerHTML = currentScore;
    let t2 = performance.now();
    console.log((t2-t1).toFixed(2));
};

plusButton.onclick = function() {
    let t1 = performance.now();
    let currentScore = getCurrentScore();
    score.innerHTML = incrementByOne(currentScore);
    let t2 = performance.now();
    console.log((t2-t1).toFixed(2));
};